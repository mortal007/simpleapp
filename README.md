# Setting Up a CI/CD Pipeline for a Simple Standalone Application

## Table of Contents

- [GitLab Repository Setup](#gitlab-repository-setup)
- [Sample Web Application](#sample-web-application)
- [Jenkins Setup](#jenkins-setup)
- [GitLab CI Configuration](#gitlab-ci-configuration)


## GitLab Repository Setup

### 1. Create a New GitLab Repository

- Utilizing [GitLab](https://gitlab.com/), a new public repository named "SimpleApp" has been created for this assignment.
- The repository is initialized with a simple README.md file.

### 2. Cloned the Repository to Your Local Machine

- Cloned the repository to local machine using the following commands:
    ```bash
    git clone https://gitlab.com/mortal007/simpleapp
    cd your-repository
    ```

---

## Sample Web Application

### 1. Develop a Simple Web Application

- The web application is built using the Node.js framework.
- Basic functionalities include the creation of a user registration form.
- The attached PDF contains screenshots of the project running successfully on localhost.

---

## Jenkins Setup

- Configured Jenkins to integrate with your GitLab repository.

### Created Jenkins Pipeline Using Jenkinsfile. 
```yaml

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                git 'https://gitlab.com/mortal007/simpleapp.git'
            }
        }

        stage('Build') {
            steps {
                
                script {
                    bat 'node --version'
                    bat 'npm install'
                }
            }
        }

        stage('Test') {
            steps {
                script {
                  bat 'echo Testing'
                }
            }
        }

        stage('Deploy') {
            steps {
                script {
                    bat 'node app.js'
                }
            }
        }
    }
}
```

---


## GitLab CI Configuration

- Created a .gitlab-ci.yml file in the root of GitLab repository.

### 2. Define CI/CD Pipeline Stages and Jobs

```yaml
stages:
    - build_stage
    - deploy_stage

build:
    stage: build_stage
    image: node
    script:
        - npm install
    artifacts:
        paths:
            - node_modules
            - package-lock.json

deploy:
    stage: deploy_stage
    image: node
    script:
        - node app.js /dev/null 2>&1 &
```

---


**Thank You!**

---
